package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.*;

import java.util.List;

public interface MovieService {

    List<MovieDto> findAll();

    void creatMovie(CreateMovieDto createMovieDto);

    void updateMovie(UpdateMovieDto updateMovieDto, Long movieId);

    void deleteMovie(Long movieId);

    CounterDto getMoviesCounter();

    DetailsMovieDto findMovie(Long movieId);
}
